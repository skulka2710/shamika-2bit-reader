package org.lorainelab.igb.sequencereader;

import aQute.bnd.annotation.component.Component;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuItem;
import org.lorainelab.igb.menu.api.model.MenuIcon;

@Component(immediate = true)
public class TwoBitReaderApp implements MenuBarEntryProvider {

    private final String ICONPATH = "summarize.png";
    private static final String MENU_TEXT = "Summarize TwoBit File";
    private static final int MENU_ITEM_WEIGHT = 5;

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem(MENU_TEXT, (Void t) -> {
            TwoBitFileSummarizer twoBitFileSummarizer = new TwoBitFileSummarizer();
            String message = twoBitFileSummarizer.getSummarizeMessage();
            JOptionPane.showMessageDialog(null, message);
            return t;
        });

        menuItem.setWeight(MENU_ITEM_WEIGHT);

        try (InputStream resourceAsStream = TwoBitReaderApp.class.getClassLoader().getResourceAsStream(ICONPATH)) {
            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
            Logger.getLogger(TwoBitReaderApp.class.getName()).log(Level.WARNING, null, ex);
        }

        return Optional.ofNullable(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }

}