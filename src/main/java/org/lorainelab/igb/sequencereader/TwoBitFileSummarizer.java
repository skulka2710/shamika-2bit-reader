/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.sequencereader;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.biojava.nbio.genome.parsers.twobit.TwoBitParser;

/**
 *
 * @author Deepti
 */
public class TwoBitFileSummarizer extends JPanel {

    private JFileChooser fileChooser;

    public TwoBitFileSummarizer() {
        //Create a file chooser
        fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }

    /**
     * This method parses the file and returns the number of sequences in it.
     *
     * @param twoBitFile
     * @return Integer
     */
    public int getSequenceCount(File twoBitFile) {

        int sequenceCount = -1;

        try {
            TwoBitParser parser = new TwoBitParser(twoBitFile);
            sequenceCount = parser.getSequenceNames().length;

        } catch (Exception ex) {
            Logger.getLogger(TwoBitReaderApp.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sequenceCount;
    }

    /**
     * This method opens a file chooser window, allows user to select a file,
     * gets number of sequences in the file and returns a message with the
     * number of sequences and the file path.
     *
     * @return String
     */
    public String getSummarizeMessage() {

        File twoBitFile;
        String twoBitFilePath;
        String message;

        JFrame fileChooserWindow = new JFrame("File Chooser Window");
        fileChooserWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        fileChooserWindow.add(this);

        fileChooser.showOpenDialog(TwoBitFileSummarizer.this);
        twoBitFilePath = fileChooser.getSelectedFile().getAbsolutePath();
        twoBitFile = new File(twoBitFilePath);

        message = "Could not read file " + twoBitFilePath;

        int count = this.getSequenceCount(twoBitFile);

        if (count == 0) {
            message = "There are no sequences in file " + twoBitFilePath;

        } else if (count != -1) {
            message = "Read " + count + " sequences from " + twoBitFilePath;
        }

        return message;
    }

}
