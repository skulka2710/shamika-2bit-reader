## App Description ##

This repository contains code for a very simple IGB App that reads and summarizes information about a TwoBit sequence file. 

### How do I run this app? ###

1. Install **TwoBit File Reader** app.
2. Select **Tools > Summarize TwoBit File** to run the App. A new dialog box will open.
3. Select a 2bit sequence file for which you want to summarize data.
4. Click on **Open**. 
5. A new dialog box will open indicating the number of sequences read or any errors in reading data.

### Questions or Comments? ###

Contact:

* Ann Loraine - aloraine@uncc.edu or aloraine@gmail.com
* Deepti Joshi - djoshi4@uncc.edu

